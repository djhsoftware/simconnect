﻿// <copyright file="WebSocketServer.cs" company="DJH Software">
// Copyright (c) DJH Software. All rights reserved.
// </copyright>
// BECAUSE YOU NEVER REMEMBER!!
// You need to update/download the SSL Certificate with Lets Encrypt (from unlimited web hosting site)
// Then you need to convert cert and key to p12 (using Ubuntu):
// openssl pkcs12 -export -in server.crt -inkey server.key -name "djhsoftware.com" -out server.p12
// Then import into Trusted Root Authorities for local computer
// Then register with app (from Admin Cmd Prompt):
// netsh http add sslcert ipport=0.0.0.0:4120 certstorename=Root certhash=26b57364fd899961a0bfd0d54270e087a7464395 appid={5d85e000-76d1-4f26-845c-c0faa7b26407}

namespace DJH.WebSocketServer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Security;
    using System.Net.WebSockets;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    public delegate void WebSocketServer_Event();
    public delegate void WebSocketServer_LogEvent(string message);

    /// <summary>
    /// Web Socker Server.
    /// </summary>
    public class WebSocketServer : IDisposable
    {
        private readonly Dictionary<string, WebSocketClient> webClients;
        private readonly string cacheDir = ".cache";
        private bool disposed = false;
        private HttpListener httpListener;
        private CancellationToken cancellationToken;
        private CancellationTokenSource cancellationTokenSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebSocketServer"/> class.
        /// </summary>
        public WebSocketServer()
        {
            // This is where we will store all connected clients
            webClients = new Dictionary<string, WebSocketClient>();
        }

        public bool Connected
        {
            get
            {
                return httpListener != null && httpListener.IsListening;
            }
        }

        /// <summary>
        /// Basic log event handler
        /// </summary>
        public event WebSocketServer_LogEvent OnLogEvent;

        /// <summary>
        /// Dispose handler
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose handler
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Execute if resources have not already been disposed.
            if (!disposed)
            {
                // If the call is from Dispose, free managed resources.
                if (disposing)
                {
                    httpListener.EndGetContext(null);
                    httpListener.Abort();
                }

                disposed = true;
            }
        }

        /// <summary>
        /// Basic broadcast of Latitude, Longitude for now
        /// </summary>
        /// <param name="data"></param>
        public async void Broadcast(dynamic data)
        {
            List<WebSocketClient> deadClients = new List<WebSocketClient>();
            foreach (WebSocketClient webClient in webClients.Values)
            {
                var result = await webClient.Send(data);
                if (!result)
                {
                    deadClients.Add(webClient);
                }
            }

            // Remove dead clients
            foreach (WebSocketClient webClient in deadClients)
            {
                webClients.Remove(webClient.Uid);
            }
        }

        #region HTTPRequestProcessors
        /// <summary>
        /// Start Web Server
        /// </summary>
        /// <param name="httpListenerPrefix">Listener Prefix</param>
        public void Start(string httpListenerPrefix)
        {
            if (!Directory.Exists(cacheDir))
            {
                Directory.CreateDirectory(cacheDir);
            }

            ServicePointManager.ServerCertificateValidationCallback += ValidationCallback;

            httpListener = new HttpListener();
            httpListener.Prefixes.Add(httpListenerPrefix);
            httpListener.Start();
            EventLog("WebServer", "Listening on " + httpListenerPrefix);

            cancellationTokenSource = new CancellationTokenSource();
            cancellationToken = cancellationTokenSource.Token;

            Task.Run(() =>
            {
                while (true)
                {
                    if (cancellationToken.IsCancellationRequested) return;

                    try
                    {
                        NonblockingListener();
                    }
                    catch (Exception e)
                    {
                        EventLog("WebServer", "Error With the listening Proccess, Message : {0}", e.Message);
                    }

                }
            }, cancellationToken);
        }

        /// <summary>
        /// Shutdown the server.
        /// </summary>
        public void Shutdown()
        {
            if (cancellationTokenSource != null) cancellationTokenSource.Cancel();

            foreach (var client in webClients)
            {
                ((WebSocketClient)client.Value).Terminate();
            }

            if (httpListener != null)
            {
                ListenerCallback(null);
                EventLog("WebServer", "Http Listening Abort");
                httpListener.Abort();
            }
        }

        /// <summary>
        /// Non blocking listener
        /// </summary>
        private void NonblockingListener()
        {
            IAsyncResult result = httpListener.BeginGetContext(ListenerCallback, httpListener);
            result.AsyncWaitHandle.WaitOne();
        }

        /// <summary>
        /// Listener callback
        /// </summary>
        /// <param name="result">Async Result</param>
        private void ListenerCallback(IAsyncResult result)
        {
            if (cancellationToken.IsCancellationRequested) return;
            httpListener = (HttpListener)result.AsyncState;
            HttpListenerContext context = httpListener.EndGetContext(result);
            if (context.Request.IsWebSocketRequest)
            {
                // WebSocket request
                ProcessWebSocketRequest(context);
            }
            else if (context.Request.HttpMethod == "POST")
            {
                ProcessHttpPostRequest(context);
            }
            else if (context.Request.HttpMethod == "GET")
            {
                ProcessHttpGetRequest(context);
            }
            else if (context.Request.HttpMethod == "OPTIONS")
            {
                ProcessOptionsRequest(context);
            }
            else
            {
                // Bad request 400
                context.Response.StatusCode = 400;
                context.Response.Close();
            }
        }

        private bool ValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// Process Web Socket request
        /// </summary>
        /// <param name="httpListenerContext">Listener Context</param>
        private async void ProcessWebSocketRequest(HttpListenerContext httpListenerContext)
        {
            WebSocketContext webSocketContext;
            try
            {
                webSocketContext = await httpListenerContext.AcceptWebSocketAsync(subProtocol: null);
                string ipAddress = httpListenerContext.Request.RemoteEndPoint.Address.ToString();
                EventLog("WebServer", "Connected: {0}", ipAddress);
            }
            catch (Exception ex)
            {
                httpListenerContext.Response.StatusCode = 500;
                httpListenerContext.Response.Close();
                EventLog("WebServer", "Exception: {0}", ex.Message);
                return;
            }

            WebSocket webSocket = webSocketContext.WebSocket;
            try
            {
                while (webSocket.State == WebSocketState.Open)
                {
                    byte[] receiveBuffer = new byte[1024];
                    Array.Clear(receiveBuffer, 0, receiveBuffer.Length);
                    WebSocketReceiveResult receiveResult = await webSocket.ReceiveAsync(new ArraySegment<byte>(receiveBuffer), CancellationToken.None);
                    if (receiveResult.MessageType == WebSocketMessageType.Close)
                    {
                        await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                        EventLog("WebServer", "Disconnected: {0}", receiveResult.CloseStatusDescription);
                    }
                    else
                    {
                        var result = Encoding.UTF8.GetString(receiveBuffer, 0, receiveBuffer.Length).TrimEnd('\0');
                        var msgBytes = Encoding.UTF8.GetBytes(ProcessMessage(webSocket, result));
                        await webSocket.SendAsync(new ArraySegment<byte>(msgBytes, 0, msgBytes.Length), WebSocketMessageType.Text, receiveResult.EndOfMessage, CancellationToken.None);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog("WebServer", "Exception: {0}", ex.Message);
            }
            finally
            {
                if (webSocket != null)
                {
                    webSocket.Dispose();
                }
            }
        }

        /// <summary>
        /// Process HTTP POST request
        /// </summary>
        /// <param name="httpListenerContext">Listener Context</param>
        private void ProcessHttpPostRequest(HttpListenerContext httpListenerContext)
        {
            try
            {
                // HTTP POST, is binary data being pushed to the server for later use
                // The POST URL MUST consist of https://<url>/action/uid
                var urlParts = httpListenerContext.Request.RawUrl.Split('/');
                var action = urlParts[1];
                var uid = urlParts[2];

                EventLog("WebServer", "[RX:POST]->Received ({0})", uid);

                // Save to cache for later
                using (StreamReader reader = new StreamReader(httpListenerContext.Request.InputStream))
                {
                    string content = reader.ReadToEnd();
                    content = WebUtility.UrlDecode(content);

                    var match = Regex.Match(content, @"data:(?<type>.+?);base64,(?<data>.+)");
                    var base64Data = match.Groups["data"].Value;
                    var contentType = match.Groups["type"].Value;
                    File.WriteAllText(cacheDir + "\\" + uid + ".txt", base64Data);

                    var byteData = Convert.FromBase64String(base64Data);
                    File.WriteAllBytes(cacheDir + "\\" + uid + ".dat", byteData);
                }

                // Success 200
                EventLog("WebServer", "[TX:POST]->200");
                httpListenerContext.Response.StatusCode = 200;
            }
            catch (Exception ex)
            {
                // Error 500
                EventLog("WebServer", "[TX:POST]->500 ({0})", ex.Message);
                httpListenerContext.Response.StatusCode = 500;
            }
            finally
            {
                httpListenerContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                httpListenerContext.Response.Close();
            }
        }

        /// <summary>
        /// Process HTTP GET request
        /// </summary>
        /// <param name="httpListenerContext">Listener Context</param>
        private void ProcessHttpGetRequest(HttpListenerContext httpListenerContext)
        {
            try
            {
                // HTTP GET, a request for binary data to be retrieved from the server that was previously POSTed
                EventLog("WebServer", "[RX:GET]->Received");

                // Screen will call GET on server to retrieve the data
                var urlParts = httpListenerContext.Request.RawUrl.Split('/');
                if (urlParts.Length > 2)
                {
                    var action = urlParts[1];
                    var uid = urlParts[2];
                    var cachefile = cacheDir + "\\" + uid + ".dat";
                    if (File.Exists(cachefile))
                    {
                        var data = File.ReadAllBytes(cachefile);
                        httpListenerContext.Response.ContentType = "image/png";
                        httpListenerContext.Response.StatusCode = 200;
                        httpListenerContext.Response.ContentLength64 = data.Length;
                        using (BinaryWriter writer = new BinaryWriter(httpListenerContext.Response.OutputStream))
                        {
                            writer.Write(data);
                        }

                        // Success 200
                        httpListenerContext.Response.StatusCode = 200;
                    }
                    else
                    {
                        // Not found 404
                        httpListenerContext.Response.StatusCode = 404;
                    }

                    EventLog("WebServer", "[TX:GET]->{0}", httpListenerContext.Response.StatusCode);
                    httpListenerContext.Response.Close();
                }
                else
                {
                    // Say Hi
                    var response = "FSX SimConnect Listener";
                    httpListenerContext.Response.StatusCode = 200;
                    httpListenerContext.Response.OutputStream.Write(Encoding.Default.GetBytes(response), 0, response.Length);
                    httpListenerContext.Response.Close();
                }
            }
            catch (Exception ex)
            {
                // Error 500
                EventLog("WebServer", "[TX:POST]->500 ({0})", ex.Message);
                httpListenerContext.Response.StatusCode = 500;
            }
            finally
            {
                httpListenerContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                httpListenerContext.Response.Close();
            }
        }

        /// <summary>
        /// Process OPTIONS request
        /// </summary>
        /// <param name="httpListenerContext">Listener Context</param>
        private void ProcessOptionsRequest(HttpListenerContext httpListenerContext)
        {
            httpListenerContext.Response.StatusCode = 200;
            EventLog("WebServer", "[TX:OPTIONS]->{0}", httpListenerContext.Response.StatusCode);
            httpListenerContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            httpListenerContext.Response.Headers.Add("Access-Control-Allow-Headers", "*");
            httpListenerContext.Response.Headers.Add("Allow", "POST, GET, OPTIONS");
            httpListenerContext.Response.Close();
        }
        #endregion

        /// <summary>
        /// Retrieves an entity for an UID
        /// </summary>
        /// <param name="uid">UID to validate</param>
        /// <returns>TRUE if UID is still valid</returns>
        private WebSocketClient GetClientForUid(string uid)
        {
            if (string.IsNullOrEmpty(uid) || !webClients.ContainsKey(uid))
            {
                return null;
            }

            var gameClient = webClients[uid];
            return gameClient;
        }

        /// <summary>
        /// Process a message and create a result
        /// </summary>
        /// <param name="socket">Source Web Socket</param>
        /// <param name="result">Result content</param>
        /// <returns>Message response</returns>
        private string ProcessMessage(WebSocket socket, string result)
        {
            try
            {
                // New connection
                EventLog("WebServer", "[RX] {0}", result);

                // deserialize the message
                dynamic jsonData = JsonConvert.DeserializeObject(result);
                dynamic processResult = null;

                string action = jsonData.action;
                string uid = jsonData.uid;

                WebSocketClient client;
                switch (action)
                {
                    case "uid":
                        // Register new client
                        client = new WebSocketClient(socket);
                        webClients.Add(client.Uid, client);
                        processResult = new { result = "uid", uid = client.Uid };
                        break;
                    case "poll":
                        // New Poll target to register
                        client = GetClientForUid(uid) as WebSocketClient;
                        if (client == null)
                        {
                            processResult = new { action = "message", result = "error", data = "No session :(" };
                            break;
                        }

                        string message = jsonData.message;
                        //await client.Room.SendHostMessage(new { action = "message", name = client.Name, message });
                        processResult = new { action = "message", result = "success" };
                        break;
                    default:
                        processResult = new { result = "error", data = "Unknown action" };
                        break;
                }

                var send = JsonConvert.SerializeObject(processResult);
                EventLog("WebServer", "[TX] {0}", send);
                return send;
            }
            catch (Exception ex)
            {
                // Just send message for now....
                var send = JsonConvert.SerializeObject(new { ident = (string)null, action = "error", msg = ex.Message });
                EventLog("WebServer", "[TX] {0}", send);
                return send;
            }
        }

        private X509Certificate2 CheckIfCertificateExists(string subjectName, StoreName storeName, StoreLocation storeLocation)
        {
            try
            {
                using (X509Store store = new X509Store(storeName, storeLocation))
                {
                    store.Open(OpenFlags.ReadOnly);
                    X509Certificate2 certificate = null;

                    var certificates = store.Certificates.Find(X509FindType.FindBySubjectName, subjectName, false);
                    if (certificates != null && certificates.Count > 0)
                    {
                        certificate = certificates[0];
                    }

                    return certificate;
                }
            }
            catch
            {
                // For now...
                throw;
            }
        }

        /// <summary>
        /// Internal event log message wrapper
        /// </summary>
        /// <param name="source">Event source</param>
        /// <param name="message">Event message</param>
        private void EventLog(string source, string message, params object[] args)
        {
            OnLogEvent?.Invoke(string.Format(source + ": " + message, args));
        }
    }
}