﻿// <copyright file="GameEntity.cs" company="DJH Software">
// Copyright (c) DJH Software. All rights reserved.
// </copyright>

namespace DJH.WebSocketServer
{
    using System;
    using System.Net.WebSockets;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Game Entity abstract class
    /// </summary>
    public class WebSocketClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebSocketClient"/> class.
        /// </summary>
        /// <param name="socket">Associated web socket</param>
        public WebSocketClient(WebSocket socket)
        {
            Socket = socket;
            Uid = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Terminate connection
        /// </summary>
        public void Terminate()
        {
            Socket.Dispose();
        }

        /// <summary>
        /// Gets the client UID
        /// </summary>
        public string Uid { get; private set; }

        /// <summary>
        /// Gets the associated web socket for this client.
        /// </summary>
        public WebSocket Socket { get; }

        /// <summary>
        /// Send Data to socket
        /// </summary>
        /// <param name="jsonData">Json Data to send</param>
        public async Task<bool> Send(dynamic jsonData)
        {
            try
            {
                JObject jsonObject = JObject.FromObject(jsonData);
                var send = JsonConvert.SerializeObject(jsonObject);
                var msgBytes = Encoding.UTF8.GetBytes(send);
                await Socket.SendAsync(new ArraySegment<byte>(msgBytes, 0, msgBytes.Length), WebSocketMessageType.Text, true, CancellationToken.None);
                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}
