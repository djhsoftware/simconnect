﻿// <copyright file="WebService.cs" company="DJH Software">
// Copyright (c) DJH Software. All rights reserved.
// </copyright>

namespace SimControl
{
    using System;
    using System.Windows;
    using System.Windows.Threading;
    using DJH.WebSocketServer;

    /// <summary>
    /// Interaction logic for WebService.xaml
    /// </summary>
    public partial class WebServiceWindow : Window
    {
        public event OnSubWindowClosed OnClose;
        private readonly WebSocketServer webServer;

        public WebServiceWindow()
        {
            InitializeComponent();

            webServer = new WebSocketServer();
            webServer.OnLogEvent += WebServer_OnLogEvent;
        }

        public void Broadcast(dynamic data)
        {
            if (webServer.Connected)
            {
                webServer.Broadcast(data);
            }
        }

        private void WebServer_OnLogEvent(string message)
        {
            displayHttpsLog.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                var newLog = string.Format("{0} {1}\n{2}", DateTime.Now, message, displayHttpsLog.Text);
                if (newLog.Length > 2000) newLog = newLog.Substring(0, 2000);
                displayHttpsLog.Text = newLog;
            }));
        }

        private void btnStartWeb_Click(object sender, RoutedEventArgs e)
        {
            webServer.Start(webServiceUrl.Text);
        }

        private void btnStopWeb_Click(object sender, RoutedEventArgs e)
        {
            webServer.Shutdown();
        }

        public void LockTo(MainWindow opener)
        {
            Left = opener.Left + opener.Width - Width;
            Top = opener.Top + opener.Height;
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            webServer.Shutdown();
            OnClose?.Invoke();
        }
    }
}
