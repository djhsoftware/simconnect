﻿using System.Windows.Markup;

namespace SimControl
{
    public class TrackInfo
    {
        public TrackInfo(string line)
        {
            var tokens = line.Split(',');
            if (tokens.Length == 13)
            {
                Time = double.Parse(tokens[0]);
                Latitude = double.Parse(tokens[1]);
                Longitude = double.Parse(tokens[2]);
                Altitude = double.Parse(tokens[3]);
                Heading = double.Parse(tokens[4]);
                Roll = double.Parse(tokens[5]);
                Pitch = double.Parse(tokens[6]);
                Yaw = double.Parse(tokens[7]);
                AOA = double.Parse(tokens[8]);
                TAS = double.Parse(tokens[9]);
                CAS = double.Parse(tokens[10]);
                IAS = double.Parse(tokens[11]);
                Mach = double.Parse(tokens[12]);
            }
        }

        public double Time { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public double Heading { get; set; }
        public double Roll { get; set; }
        public double Pitch { get; set; }
        public double Yaw { get; set; }
        public double AOA { get; set; }
        public double TAS { get; set; }
        public double CAS { get; set; }
        public double IAS { get; set; }
        public double Mach { get; set; }
    }
}