﻿// <copyright file="ScreenOverlay.cs" company="DJH Software">
// Copyright (c) DJH Software. All rights reserved.
// </copyright>

namespace SimControl
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for ScreenOverlay.xaml
    /// </summary>
    public partial class InfoWindow : Window
    {
        public event OnSubWindowClosed OnClose;
        public readonly MainWindow owner;

        public InfoWindow(MainWindow opener)
        {
            InitializeComponent();
            owner = opener;
            LockTo(owner);
        }

        public double ClientWidth
        {
            get { return ((FrameworkElement)this.Content).ActualWidth; }
        }

        public double ClientHeight
        {
            get { return ((FrameworkElement)this.Content).ActualHeight; }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            /*
            var requiredRatio = 1920.0 / 1080.0;
            var dHeight = Height - ClientHeight;
            var dWidth = Width - ClientWidth;

            var aWidth = Height * requiredRatio;
            var aHeight = Width * requiredRatio;
            if (Math.Abs(aHeight - Height) > Math.Abs(aWidth - Width))
            {
                Width = aWidth - dWidth;
            }
            else
            {
                Height = aHeight - dHeight;
            }
            */
        }

        public void LockTo(MainWindow opener)
        {
            Left = opener.Left + opener.Width - Width;
            Top = opener.Top + opener.Height;
        }

        internal void Update(string text)
        {
            overlayInfo.Text = text;
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            OnClose?.Invoke();
        }
    }
}
