﻿// <copyright file="MainWindow.cs" company="DJH Software">
// Copyright (c) DJH Software. All rights reserved.
// </copyright>

namespace SimControl
{
    using DJH.SimConnectWrapper;
    using SimConnect;
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Windows.Interop;
    using System.Windows.Media;
    using System.Windows.Threading;

    public delegate void OnSubWindowClosed();

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MapViewWindow mapViewWindow = null;
        public InfoWindow infoWindow = null;
        public WebServiceWindow webServiceWindow = null;

        private readonly DispatcherTimer fastTimer;
        private readonly DispatcherTimer slowTimer;
        private static ISimConnectController controller = null;
        private int tick;

        private FlightTracker flightTracker = null;
        private FlightTracker blackBox = null;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = this;

            fastTimer = new DispatcherTimer();
            fastTimer.Tick += Timer_Tick; ;
            fastTimer.Interval = new TimeSpan(0, 0, 1);

            slowTimer = new DispatcherTimer();
            slowTimer.Tick += SlowTimer_Tick;
            slowTimer.Interval = new TimeSpan(0, 0, 30);

            if (!Utility.UserIsAdmin())
            {
                btnViewWebServer.IsEnabled = false;
                btnViewWebServer.ToolTip = "Web server requires Admin privileges";
            }

            flightTracker = new FlightTracker("track");
            flightTracker.OnMessage += Controller_logEvent;
            flightTracker.OnRunStateChange += FlightTracker_OnRunStateChange;
            blackBox = new FlightTracker("blackbox");
            blackBox.OnMessage += Controller_logEvent;
            blackBox.OnRunStateChange += FlightTracker_OnRunStateChange;
        }

        private void FlightTracker_OnRunStateChange(bool running)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FlightTrackerButtonColour"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("BlackBoxButtonColour"));
        }

        public Brush FlightTrackerButtonColour
        {
            get
            {
                return flightTracker.Running ? Brushes.LightGreen : Brushes.LightGray;
            }
        }

        public Brush BlackBoxButtonColour
        {
            get
            {
                return blackBox.Running ? Brushes.LightGreen : Brushes.LightGray;
            }
        }

        public Brush ConnectedButtonColour
        {
            get
            {
                return controller != null && controller.Connected ? Brushes.LightGreen : Brushes.LightGray;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (controller == null || !controller.Connected) return;
            controller.Poll();

            UpdateDisplay();

            blackBox.Write(controller.SlowUpdate);

            if (mapViewWindow != null) mapViewWindow.UpdateMap(controller.SlowUpdate.Latitude, controller.SlowUpdate.Longitude, controller.SlowUpdate.HeadingDegrees, chkTrackHeading.IsChecked.GetValueOrDefault());
            if (webServiceWindow != null) webServiceWindow.Broadcast(new { tick = ++tick, latitude = controller.SlowUpdate.Latitude, longitude = controller.SlowUpdate.Longitude, headingRadians = controller.SlowUpdate.HeadingRadians });
        }

        private void SlowTimer_Tick(object sender, EventArgs e)
        {
            flightTracker.Write(controller.SlowUpdate);
        }

        private void Controller_logEvent(string message)
        {
            displayLogs.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                var newLog = string.Format("{0} {1}\n{2}", DateTime.Now, message, displayLogs.Text);
                if (newLog.Length > 2000) newLog = newLog.Substring(0, 2000);
                displayLogs.Text = newLog;
            }));
        }

        /// <summary>
        /// Quick and dirty debug display for now...
        /// </summary>
        private void UpdateDisplay()
        {
            displayEvents.Text = string.Format("Lat: {0:0.0000}  |  Lon: {0:0.0000}\n", controller.SlowUpdate.Latitude, controller.SlowUpdate.Longitude);
            File.WriteAllText("data.txt", displayEvents.Text);

            var sb = new StringBuilder();
            sb.AppendFormat("Latitude: {0:0.0000}\n", controller.SlowUpdate.Latitude);
            sb.AppendFormat("Longitude: {0:0.0000}\n", controller.SlowUpdate.Longitude);
            sb.AppendFormat("Altitude: {0:0.00}\n", controller.SlowUpdate.Altitude);
            sb.AppendFormat("IAS: {0:0.00}\n", controller.SlowUpdate.IndicatedAirSpeed);
            sb.AppendFormat("Heading: {0:0.00}°\n", controller.SlowUpdate.HeadingDegrees);
            displayEvents.Text = sb.ToString();

            if (infoWindow != null) infoWindow.Update(displayEvents.Text);
        }

        #region Window Events
        private void MapViewWindow_OnClose()
        {
            mapViewWindow = null;
            btnViewMap.IsChecked = false;
        }

        private void InfoWindow_OnClose()
        {
            infoWindow = null;
            btnViewInfoWindow.IsChecked = false;
        }

        private void WebServiceWindow_OnClose()
        {
            webServiceWindow = null;
            btnViewWebServer.IsChecked = false;
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (chkEmulated.IsChecked.GetValueOrDefault())
                controller = new SimConnectEmulatedController("DJH FSX Test App");
            else
                controller = new SimConnectController("DJH FSX Test App");

            controller.OnLogEvent += Controller_logEvent;

            WindowInteropHelper lWih = new WindowInteropHelper(this);
            IntPtr lHwnd = lWih.Handle;
            controller.Connect(lHwnd);

            HwndSource source = HwndSource.FromHwnd(lHwnd);
            source.AddHook(new HwndSourceHook(WndProc));

            fastTimer.Start();
            slowTimer.Start();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ConnectedButtonColour"));
        }

        private static IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            handled = controller.HandleMessage(msg);
            return IntPtr.Zero;
        }

        private void btnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            if (controller == null) return;

            controller.Close();
            displayEvents.Text = string.Empty;

            slowTimer.Stop();
            fastTimer.Stop();

            blackBox.Stop();
            flightTracker.Stop();

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ConnectedButtonColour"));
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            if (infoWindow != null) infoWindow.Close();
            if (mapViewWindow != null) mapViewWindow.Close();
            if (webServiceWindow != null) webServiceWindow.Close();
            File.WriteAllText("data.txt", "");
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (mapViewWindow != null) mapViewWindow.LockTo(this);
            if (infoWindow != null) infoWindow.LockTo(this);
            if (webServiceWindow != null) webServiceWindow.LockTo(this);
        }

        private void Window_LocationChanged(object sender, EventArgs e)
        {
            if (mapViewWindow != null) mapViewWindow.LockTo(this);
            if (infoWindow != null) infoWindow.LockTo(this);
            if (webServiceWindow != null) webServiceWindow.LockTo(this);
        }

        private void chkShowDebug_Click(object sender, RoutedEventArgs e)
        {
            if (mapViewWindow != null) mapViewWindow.ShowControls(chkShowDebug.IsChecked.GetValueOrDefault());
        }

        private void btnViewMap_Click(object sender, RoutedEventArgs e)
        {
            if (btnViewMap.IsChecked.GetValueOrDefault())
            {
                if (mapViewWindow == null)
                {
                    mapViewWindow = new MapViewWindow(this);
                    mapViewWindow.OnClose += MapViewWindow_OnClose;
                }

                mapViewWindow.Show();
                mapViewWindow.BringIntoView();
                mapViewWindow.Tracker = blackBox;
                if (mapViewWindow.WindowState == WindowState.Minimized) mapViewWindow.WindowState = WindowState.Normal;
            }
            else
            {
                if (mapViewWindow != null) mapViewWindow.Close();
            }
        }

        private void btnViewInfoWindow_Click(object sender, RoutedEventArgs e)
        {
            if (btnViewInfoWindow.IsChecked.GetValueOrDefault())
            {
                if (infoWindow == null)
                {
                    infoWindow = new InfoWindow(this);
                    infoWindow.OnClose += InfoWindow_OnClose;
                }

                infoWindow.Show();
                infoWindow.BringIntoView();
                if (infoWindow.WindowState == WindowState.Minimized) infoWindow.WindowState = WindowState.Normal;
            }
            else
            {
                if (infoWindow != null) infoWindow.Close();
            }
        }

        private void btnViewWebServer_Click(object sender, RoutedEventArgs e)
        {
            if (btnViewWebServer.IsChecked.GetValueOrDefault())
            {
                if (webServiceWindow == null)
                {
                    webServiceWindow = new WebServiceWindow();
                    webServiceWindow.OnClose += WebServiceWindow_OnClose;
                }

                webServiceWindow.Show();
                webServiceWindow.BringIntoView();
                if (webServiceWindow.WindowState == WindowState.Minimized) webServiceWindow.WindowState = WindowState.Normal;
            }
            else
            {
                if (webServiceWindow != null) webServiceWindow.Close();
            }

        }

        private void FlightTrackingBtn_Click(object sender, RoutedEventArgs e)
        {
            flightTracker.Toggle();
        }

        private void BlackBoxBtn_Click(object sender, RoutedEventArgs e)
        {
            blackBox.Toggle();
        }

        #endregion
    }
}
