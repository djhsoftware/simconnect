﻿
namespace SimControl
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using DJH.SimConnectWrapper;

    public class FlightTracker : IDisposable
    {
        public delegate void FlightTracker_Message(string message);
        public delegate void FlightTracker_StateChange(bool running);

        /// <summary>
        /// Is the instance disposed?
        /// </summary>
        protected bool disposed = false;
        private string trackName;
        private StreamWriter streamWriter = null;
        private DateTime streamStart;
        public event FlightTracker_Message OnMessage;
        public event FlightTracker_StateChange OnRunStateChange;

        private TrackInfo[] trackInfo;
        private int trackPosition = 0;
        private TimeSpan trackLength;
        private TimeSpan trackTime;

        public FlightTracker(string name)
        {
            trackName = name;
        }

        public void Load(string filename)
        {
            bool firstLine = true;
            var list = new List<TrackInfo>();
            using (var sr = new StreamReader(filename))
            {
                var line = sr.ReadLine();
                while (line != null)
                {
                    if (firstLine)
                    {
                        firstLine = false;
                    }
                    else
                    {
                        list.Add(new TrackInfo(line));
                    }

                    line = sr.ReadLine();
                }
            }

            trackInfo = list.ToArray();
            trackLength = TimeSpan.FromSeconds(trackInfo.Max(t => t.Time));
            list.Clear();
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool Running
        {
            get
            {
                return streamWriter != null;
            }
        }

        public TimeSpan Position
        {
            get
            {
                return trackTime;
            }
        }

        public TimeSpan Length
        {
            get
            {
                return trackLength;
            }
        }

        public string TimeCode
        {
            get
            {
                return string.Format("{0:mm\\:ss} / {1:mm\\:ss}", Position, Length);
            }
        }

        /// <summary>
        /// Protected Dispose
        /// </summary>
        /// <param name="disposing">Disposing flag</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                Stop();
            }

            disposed = true;
        }

        public void Toggle()
        {
            if (Running)
            {
                Stop();
            }
            else
            {
                Start();
            }
        }

        internal TrackInfo Next()
        {
            trackPosition++;
            if (trackPosition < trackInfo.Length)
            {
                var trackData = trackInfo[trackPosition];
                trackTime = TimeSpan.FromSeconds(trackData.Time);
                return trackData;
            }
            else
            {
                return null;
            }
        }

        public void Start()
        {
            if (Running) return;
            streamStart = DateTime.UtcNow;
            var filename = string.Format("{0}_{1:yyyymmddhhmmss}.csv", trackName, streamStart);
            streamWriter = new StreamWriter(filename, false);
            streamWriter.WriteLine("Time,Latitude,Longitude,Altitude,Heading,Roll,Pitch,Yaw,AOA,TAS,CAS,IAS,Mach");
            OnMessage?.Invoke(" FlightTracker '" + trackName + "' started");
            OnRunStateChange?.Invoke(true);
        }

        public void Stop()
        {
            if (!Running) return;
            streamWriter.BaseStream.Flush();
            streamWriter.Close();
            streamWriter.Dispose();
            streamWriter = null;
            OnMessage?.Invoke(" FlightTracker '" + trackName + "' stopped");
            OnRunStateChange?.Invoke(false);
        }

        public void Write(SimDataFastUpdate update)
        {
            if (!Running) return;
            var elapsed = DateTime.UtcNow - streamStart;
            streamWriter.WriteLine("{0:0.00},{1},{2},{3},{4:0.00},{5:0.00},{6:0.00},{7:0.00},{8:0.00},{9:0.00},{10:0.00},{11:0.00},{12:0.00}",
                elapsed.Seconds,
                update.Latitude,
                update.Longitude,
                update.Altitude,
                update.HeadingDegrees,
                update.BankDegrees,
                update.PitchDegrees,
                update.YawDegrees,
                update.AngleOfAttackDegrees,
                update.TrueAirSpeed,
                update.CalibratedAirSpeed,
                update.IndicatedAirSpeed,
                update.Mach);

            trackLength = trackTime = TimeSpan.FromSeconds(elapsed.Seconds);
        }

        internal TrackInfo Peek()
        {
            return trackInfo[trackPosition];
        }
    }
}
