﻿// <copyright file="MapViewer.cs" company="DJH Software">
// Copyright (c) DJH Software. All rights reserved.
// </copyright>

namespace SimControl
{
    using GMap.NET;
    using GMap.NET.MapProviders;
    using GMap.NET.WindowsPresentation;
    using Microsoft.Win32;
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using System.Windows.Threading;
    using Brushes = System.Windows.Media.Brushes;
    using Point = System.Windows.Point;

    /// <summary>
    /// Interaction logic for MapViewer.xaml
    /// </summary>
    public partial class MapViewWindow : Window
    {
        public event OnSubWindowClosed OnClose;
        public readonly MainWindow owner;
        public FlightTracker Tracker = null;
        private readonly GMapMarker flightMarker;
        private readonly Shape shape;
        private readonly PointCollection markerpoints = new PointCollection { new Point(-5, 10), new Point(0, -10), new Point(5, 10) };
        private readonly Point pivot = new Point(0, 0);

        private DispatcherTimer playbackTimer = null;

        public MapViewWindow(MainWindow opener)
        {
            InitializeComponent();

            owner = opener;
            LockTo(owner);

            MapOpenStreet.MapProvider = GMapProviders.OpenStreetMap;
            
            shape = new Polygon
            {
                Points = markerpoints,
                Stroke = Brushes.Black,
                Fill = Brushes.Black,
                StrokeThickness = 1.5
            };

            /*shape = new Ellipse
            {
                Stroke = Brushes.Black,
                Fill = Brushes.Black,
                StrokeThickness = 1
            };*/

            flightMarker = new GMapMarker(new PointLatLng())
            {
                Shape = shape
            };

            MapOpenStreet.Markers.Clear();
            MapOpenStreet.Markers.Add(flightMarker);

            MapOpenStreet.ShowCenter = false;
            MapOpenStreet.HelperLineOption = HelperLineOptions.DontShow;

            playbackTimer = new DispatcherTimer();
            playbackTimer.Interval = new TimeSpan(0, 0, 1);
            playbackTimer.Tick += PlaybackTimer_Tick;
        }

        private void PlaybackTimer_Tick(object sender, EventArgs e)
        {
            var trackInfo = Tracker.Next();
            if (trackInfo != null)
            {
                UpdateMapView(trackInfo.Latitude, trackInfo.Longitude, trackInfo.Heading, false);
            }
            else
            {
                playbackTimer.Stop();
            }
        }

        public void UpdateMap(double latitude, double longitude, double heading, bool trackHeading)
        {
            if (playbackTimer.IsEnabled)
            {
                return;
            }

            UpdateMapView(latitude, longitude, heading, trackHeading);
        }

        public void ShowControls(bool showDebug)
        {
            DebugBar.Visibility = showDebug ? Visibility.Visible : Visibility.Collapsed;
            Toolbar.Visibility = DebugBar.Visibility;
        }

        public void LockTo(MainWindow opener)
        {
            Left = opener.Left + opener.Width;
            Top = opener.Top;
        }

        private void UpdateMapView(double latitude, double longitude, double heading, bool trackHeading)
        {
            var position = new PointLatLng(latitude, longitude);
            flightMarker.Position = position;

            var markerHeading = trackHeading ? 0.0f : (float)heading;
            var mapBearing = trackHeading ? (float)heading : 0.0f;

            if (shape is Polygon polygon)
            {
                polygon.Points = FromRotation(markerpoints, pivot, markerHeading);
            }

            MapOpenStreet.Bearing = mapBearing;
            MapOpenStreet.Position = position;

            mapDebug.Text = string.Format("Map Bearing: {0} | Marker: {1} | Position: {2}", mapBearing, markerHeading, position);
            playbackTime.Text = Tracker.TimeCode;
        }

        private PointCollection FromRotation(PointCollection points, Point pivot, double angleDegree)
        {
            double angle = angleDegree * Math.PI / 180;
            double cos = Math.Cos(angle);
            double sin = Math.Sin(angle);

            PointCollection rotated = new PointCollection(points.Count);

            foreach (var point in points)
            {
                double dx = point.X - pivot.X;
                double dy = point.Y - pivot.Y;

                double x = (cos * dx) - (sin * dy) + pivot.X;
                double y = (sin * dx) + (cos * dy) + pivot.Y;

                rotated.Add(new Point((double)x, (double)y));
            }

            return rotated;
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            OnClose?.Invoke();
        }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                Title = "Open track file",
                Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*"
            };

            var result = dialog.ShowDialog();
            if (result.GetValueOrDefault())
            {
                var filename = dialog.FileName;
                Tracker = new FlightTracker(filename);
                Tracker.Load(filename);
                var trackInfo = Tracker.Peek();
                UpdateMapView(trackInfo.Latitude, trackInfo.Longitude, trackInfo.Heading, false);
            }
        }

        private void btnClearMap_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnTrack_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (!playbackTimer.IsEnabled)
            {
                playbackTimer.Start();
            }

            btnPlay.IsChecked = playbackTimer.IsEnabled;
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            playbackTimer.Stop();
            btnPlay.IsChecked = playbackTimer.IsEnabled;
        }

        private void btnBackward_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnForward_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSaveVideo_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
