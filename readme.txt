SimConnect docs
===============

https://docs.microsoft.com/en-us/previous-versions/microsoft-esp/cc526983(v=msdn.10)

Setting up Secure Web Sockets
=============================

- Create SSL Certificate with Lets Encrypt

- Convert cert and key to p12:
	openssl pkcs12 -export -in server.crt -inkey server.key -name "domainname.com" -out server.p12

- Import into Trusted Root Authorities for local computer

- Register with app (from Admin Cmd Prompt):
	netsh http add sslcert ipport=0.0.0.0:4120 certstorename=Root certhash=<thumbprint> appid={5d85e000-76d1-4f26-845c-c0faa7b26407}

