﻿namespace SimConnect
{
    using System;
    using System.Windows.Forms;
    using DJH.SimConnectWrapper;

    /// <summary>
    /// Sim Connect Controller Wrapper
    /// 
    /// SimConnect docs are here:
    /// https://docs.microsoft.com/en-us/previous-versions/microsoft-esp/cc526980(v=msdn.10)
    /// </summary>
    public class SimConnectEmulatedController : SimConnectControllerBase, ISimConnectController
    {
        private bool connected = false;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of application to register with FSX</param>
        public SimConnectEmulatedController(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Get a value indicating whether SimConnect is connected to the simulation
        /// </summary>
        public override bool Connected
        {
            get
            {
                return connected;
            }
        }

        /// <summary>
        /// Handle window message
        /// </summary>
        /// <param name="m">message</param>
        /// <returns>true if handled</returns>
        public override bool HandleMessage(ref Message m)
        {
            return false;
        }

        /// <summary>
        /// Handle window message
        /// </summary>
        /// <param name="msg">message</param>
        /// <returns>true if handled</returns>
        public override bool HandleMessage(int msg)
        {
            return false;
        }

        /// <summary>
        /// Connect to FSX
        /// </summary>
        /// <param name="hWnd">Window handle to receive events</param>
        public override void Connect(IntPtr hWnd)
        {
            EventLog("Connect", "[Emulated] Connecting...");
            connected = true;
            SlowUpdate.Emulate(true);
            ManualUpdate.Emulate(true);
        }

        /// <summary>
        /// Temporary poller
        /// Will be replaced with internal thread at some point...
        /// </summary>
        public override void Poll()
        {
            SlowUpdate.Emulate(false);
        }

        /// <summary>
        /// Request manual update
        /// </summary>
        public override void ExecuteManualUpdate()
        {
            ManualUpdate.Emulate(false);
        }

        /// <summary>
        /// Close connection to FSX
        /// </summary>
        public override void Close()
        {
            connected = false;
        }
    }
}
