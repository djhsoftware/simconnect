﻿
namespace DJH.SimConnectWrapper
{
    using System;
    using System.Runtime.InteropServices;
    using Microsoft.FlightSimulator.SimConnect;

    public class SimDataManualUpdate : ISimDataUpdate
    {
        /// <summary>
        /// Data structure for information that is received only on demand
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        internal struct ManualUpdateStruct
        {
            public double ZuluTime;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string Title;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string Type;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string Model;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string FlightNumber;
        };

        public double ZuluTime { get; private set; }
        public string Title { get; private set; }
        public string Type { get; private set; }
        public string Model { get; private set; }
        public string FlightNumber { get; private set; }

        public event SimController_UpdateEvent OnUpdate;

        public void RegisterUpdate(SimConnect simconnect)
        {
            simconnect.AddToDataDefinition(DataStructure.ManualUpdate, "ZULU TIME", null, SIMCONNECT_DATATYPE.STRING256, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.ManualUpdate, "TITLE", null, SIMCONNECT_DATATYPE.STRING256, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.ManualUpdate, "ATC TYPE", null, SIMCONNECT_DATATYPE.STRING256, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.ManualUpdate, "ATC MODEL", null, SIMCONNECT_DATATYPE.STRING256, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.ManualUpdate, "ATC FLIGHT NUMBER", null, SIMCONNECT_DATATYPE.STRING256, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.RegisterDataDefineStruct<ManualUpdateStruct>(DataStructure.ManualUpdate);
        }

        public void Emulate(bool init)
        {
            ZuluTime = DateTime.UtcNow.Ticks;
            Title = "Title";
            Type = "Type";
            Model = "mu1.Model";
            FlightNumber = "FlightNumber";

            OnUpdate?.Invoke();
        }

        public void Update(object[] data)
        {
            ManualUpdateStruct mu1 = (ManualUpdateStruct)data[0];

            ZuluTime = mu1.ZuluTime;
            Title = mu1.Title;
            Type = mu1.Type;
            Model = mu1.Model;
            FlightNumber = mu1.FlightNumber;

            OnUpdate?.Invoke();
        }
    }
}
