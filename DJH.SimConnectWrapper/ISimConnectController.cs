﻿namespace DJH.SimConnectWrapper
{
    using System;
    using System.Windows.Forms;

    public delegate void SimController_BasicEvent(string message);
    public delegate void SimController_UpdateEvent();

    /// <summary>
    /// Sim Connect Controller Wrapper
    /// 
    /// SimConnect docs are here:
    /// https://docs.microsoft.com/en-us/previous-versions/microsoft-esp/cc526980(v=msdn.10)
    /// </summary>
    public interface ISimConnectController : IDisposable
    {
        /// <summary>
        /// Slow update handler
        /// </summary>
        SimDataFastUpdate SlowUpdate { get; }

        /// <summary>
        /// Manual update handler
        /// </summary>
        SimDataManualUpdate ManualUpdate { get; }

        /// <summary>
        /// Get a value indicating whether SimConnect is connected to the simulation
        /// </summary>
        bool Connected { get; }

        /// <summary>
        /// Basic log event handler
        /// </summary>
        event SimController_BasicEvent OnLogEvent;

        /// <summary>
        /// Manual update event handler
        /// </summary>
        event SimController_UpdateEvent OnManualUpdate;

        /// <summary>
        /// Slow update event handler
        /// </summary>
        event SimController_UpdateEvent OnSlowUpdate;

        /// <summary>
        /// Handle window message
        /// </summary>
        /// <param name="m">message</param>
        /// <returns>true if handled</returns>
        bool HandleMessage(ref Message m);

        /// <summary>
        /// Handle window message
        /// </summary>
        /// <param name="msg">message</param>
        /// <returns>true if handled</returns>
        bool HandleMessage(int msg);

        /// <summary>
        /// Connect to FSX
        /// </summary>
        /// <param name="hWnd">Window handle to receive events</param>
        void Connect(IntPtr hWnd);

        /// <summary>
        /// Temporary poller
        /// Will be replaced with internal thread at some point...
        /// </summary>
        void Poll();

        /// <summary>
        /// Close connection to FSX
        /// </summary>
        void Close();
    }
}
