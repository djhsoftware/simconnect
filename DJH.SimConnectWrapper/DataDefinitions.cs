﻿using Microsoft.VisualBasic.FileIO;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SimConnect
{
    public class SimVariable
    {
        public SimVariable(string[] parts)
        {
            Name = parts[0];
            Description = parts[1];
            Units = parts[2].Split(',');
            Settable = parts[3] == "Y";
        }

        public string Name { get; private set; }
        public string Description { get; private set; }
        public string[] Units { get; private set; }
        public bool Settable { get; private set; }
    }

    /// <summary>
    /// Data definitions object
    /// </summary>
    public sealed class DataDefinitions
    {
        private List<SimVariable> variables;
        private static DataDefinitions instance = null;

        public static DataDefinitions Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataDefinitions();
                }
                return instance;
            }
        }

        private DataDefinitions()
        {
            variables = new List<SimVariable>();
            var defs = Utility.GetEmbeddedResource("SimConnectWrapper.Resources.dataDefinitions.dat");
            using (Stream stream = new MemoryStream(defs))
            using (TextFieldParser parser = new TextFieldParser(stream, Encoding.UTF8))
            {
                parser.Delimiters = new string[] { "|" };
                while (true)
                {
                    // Read record
                    string[] parts = parser.ReadFields();
                    if (parts == null)
                    {
                        break;
                    }

                    if (parts.Length == 5)
                    {
                        variables.Add(new SimVariable(parts));
                    }
                }
            }
        }
    }
}
