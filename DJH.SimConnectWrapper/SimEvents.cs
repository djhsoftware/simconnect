﻿
namespace SimConnectWrapper
{
    internal class SimEvents
    {
        internal enum NotificationGroup
        {
            GROUP0,
        };

        internal enum SimEvent
        {
            KEY_SIM_RESET,
            EVENT_PAUSE,
            EVENT_BRAKES,
            FLAPS_INC,
            FLAPS_DEC,
            FLAPS_UP,
            FLAPS_DOWN,
        };
    }
}
