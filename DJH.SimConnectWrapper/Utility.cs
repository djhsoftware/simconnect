﻿namespace SimConnect
{
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security.Principal;

    public static class Utility
    {
        /// <summary>
        /// Get an embedded resource
        /// </summary>
        /// <param name="filename">Filename of resource</param>
        /// <param name="assembly">Assembly in which to locate resource</param>
        /// <returns>Resource as string</returns>
        public static byte[] GetEmbeddedResource(string filename, Assembly assembly = null)
        {
            try
            {
                if (assembly == null)
                {
                    // Look in executing assembly first
                    assembly = Assembly.GetExecutingAssembly();
                    if (!GetResourceNames(assembly).Contains(filename))
                    {
                        // Look in entry assembly
                        assembly = Assembly.GetEntryAssembly();
                        if (!GetResourceNames(assembly).Contains(filename))
                        {
                            return null;
                        }
                    }
                }

                // Pull in the assembly
                using (Stream stream = assembly.GetManifestResourceStream(filename))
                {
                    using (var memstream = new MemoryStream())
                    {
                        stream.CopyTo(memstream);
                        return memstream.ToArray();
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get list of resource names
        /// </summary>
        /// <param name="assembly">Assembly in which to locate resources</param>
        /// <returns>string array of resource names</returns>
        public static string[] GetResourceNames(Assembly assembly = null)
        {
            try
            {
                if (assembly == null)
                {
                    assembly = Assembly.GetExecutingAssembly();
                }

                var names = assembly.GetManifestResourceNames();
                return names;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Check if user is running as administrator
        /// </summary>
        /// <returns>true if admin</returns>
        public static bool UserIsAdmin()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
