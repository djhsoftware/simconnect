﻿
namespace DJH.SimConnectWrapper
{
    using System;
    using System.Runtime.InteropServices;
    using Microsoft.FlightSimulator.SimConnect;

    public class SimDataFastUpdate : ISimDataUpdate
    {
        /// <summary>
        /// Data structure for received data every 500ms
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        internal struct FastUpdateStruct
        {
            public double latitude;
            public double longitude;
            public double altitude;
            public double ind_as;
            public double true_as;
            public double calibrated_as;
            public double mach_as;
            public double headingRadians;
            public double planePitchRadians;
            public double planeBankRadians;
            public double IncidenceAlpha;
        };

        public double Latitude { get; protected set; }
        public double Longitude { get; protected set; }
        public double Altitude { get; protected set; }

        public double IndicatedAirSpeed { get; protected set; }
        public double TrueAirSpeed { get; protected set; }
        public double CalibratedAirSpeed { get; protected set; }
        public double Mach { get; protected set; }

        public double HeadingRadians { get; protected set; }
        public double HeadingDegrees { get; protected set; }
        public double PitchRadians { get; protected set; }
        public double PitchDegrees { get; protected set; }
        public double YawRadians { get; protected set; }    // Not found
        public double YawDegrees { get; protected set; }    // Not found
        public double BankRadians { get; protected set; }
        public double BankDegrees { get; protected set; }

        public double AngleOfAttackDegrees { get; protected set; }

        public event SimController_UpdateEvent OnUpdate;

        public void RegisterUpdate(SimConnect simconnect)
        {
            // define slow update structure
            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "PLANE LATITUDE", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "PLANE LONGITUDE", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "PLANE ALTITUDE", "feet", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);

            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "AIRSPEED INDICATED", "knots", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "AIRSPEED TRUE", "knots", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "AIRSPEED TRUE CALIBRATE", "knots", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "AIRSPEED MACH", "mach", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);

            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "HEADING INDICATOR", "radians", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "PLANE PITCH DEGREES", "radians", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "PLANE BANK DEGREES", "radians", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);

            simconnect.AddToDataDefinition(DataStructure.FastUpdate, "INCIDENCE ALPHA", "radians", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);

            simconnect.RegisterDataDefineStruct<FastUpdateStruct>(DataStructure.FastUpdate);
        }

        public void Emulate(bool init)
        {
            if (init)
            {
                Latitude = 51.785079f;
                Longitude = -0.200912f;
                HeadingRadians = 0.0f;
                HeadingDegrees = 0.0f;
            }
            else
            {
                Latitude += 0.001;
                Longitude += 0.001;
                Altitude = 1000;
                IndicatedAirSpeed = 0;
                TrueAirSpeed = 0;
                HeadingRadians += 0.01; if (HeadingRadians > 6.28) HeadingRadians -= 6.28;
                HeadingDegrees = HeadingRadians * (180.0 / Math.PI);
            }

            OnUpdate?.Invoke();
        }

        public void Update(object[] data)
        {
            FastUpdateStruct su1 = (FastUpdateStruct)data[0];

            Latitude = su1.latitude;
            Longitude = su1.longitude;
            Altitude = su1.altitude;

            IndicatedAirSpeed = su1.ind_as;
            TrueAirSpeed = su1.true_as;
            CalibratedAirSpeed = su1.calibrated_as;
            Mach = su1.mach_as;

            HeadingRadians = su1.headingRadians;
            HeadingDegrees = su1.headingRadians * (180.0 / Math.PI);
            PitchRadians = su1.planePitchRadians;
            PitchDegrees = su1.planePitchRadians * (180.0 / Math.PI);
            BankRadians = su1.planeBankRadians;
            BankDegrees = su1.planeBankRadians * (180.0 / Math.PI);

            AngleOfAttackDegrees = su1.IncidenceAlpha * (180.0 / Math.PI);

            OnUpdate?.Invoke();
        }
    }
}
