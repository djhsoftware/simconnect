﻿namespace SimConnect
{
    using System;
    using System.Windows.Forms;
    using DJH.SimConnectWrapper;

    /// <summary>
    /// Sim Connect Controller Wrapper
    /// 
    /// SimConnect docs are here:
    /// https://docs.microsoft.com/en-us/previous-versions/microsoft-esp/cc526980(v=msdn.10)
    /// </summary>
    public abstract class SimConnectControllerBase
    {
        protected SimConnectControllerBase(string name)
        {
            appName = name;
            ManualUpdate = new SimDataManualUpdate();
            ManualUpdate.OnUpdate += ManualUpdateHandler_OnUpdate;
            SlowUpdate = new SimDataFastUpdate();
            SlowUpdate.OnUpdate += SlowUpdateHandler_OnUpdate;
        }

        private void SlowUpdateHandler_OnUpdate()
        {
            OnSlowUpdate?.Invoke();
        }

        private void ManualUpdateHandler_OnUpdate()
        {
            OnManualUpdate?.Invoke();
        }

        /// <summary>
        /// Manual update handler
        /// </summary>
        public SimDataManualUpdate ManualUpdate { get; }

        /// <summary>
        /// Slow update handler
        /// </summary>
        public SimDataFastUpdate SlowUpdate { get; }

        /// <summary>
        /// The application name to register with FSX
        /// </summary>
        protected string appName;

        /// <summary>
        /// Is the instance disposed?
        /// </summary>
        protected bool disposed = false;

        /// <summary>
        /// Get a value indicating whether SimConnect is connected to the simulation
        /// </summary>
        public abstract bool Connected { get; }

        /// <summary>
        /// Basic log event handler
        /// </summary>
        public event SimController_BasicEvent OnLogEvent;

        /// <summary>
        /// Manual update event handler
        /// </summary>
        public event SimController_UpdateEvent OnManualUpdate;

        /// <summary>
        /// Slow update event handler
        /// </summary>
        public event SimController_UpdateEvent OnSlowUpdate;

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected Dispose
        /// </summary>
        /// <param name="disposing">Disposing flag</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                Close();
            }

            disposed = true;
        }

        /// <summary>
        /// Handle window message
        /// </summary>
        /// <param name="m">message</param>
        /// <returns>true if handled</returns>
        public abstract bool HandleMessage(ref Message m);

        /// <summary>
        /// Handle window message
        /// </summary>
        /// <param name="msg">message</param>
        /// <returns>true if handled</returns>
        public abstract bool HandleMessage(int msg);

        /// <summary>
        /// Connect to FSX
        /// </summary>
        /// <param name="hWnd">Window handle to receive events</param>
        public abstract void Connect(IntPtr hWnd);

        /// <summary>
        /// Temporary poller
        /// Will be replaced with internal thread at some point...
        /// </summary>
        public abstract void Poll();

        /// <summary>
        /// Close connection to FSX
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Execute manual update
        /// </summary>
        public abstract void ExecuteManualUpdate();

        /// <summary>
        /// Internal event log message wrapper
        /// </summary>
        /// <param name="source">Event source</param>
        /// <param name="message">Event message</param>
        protected void EventLog(string source, string message, params object[] args)
        {
            OnLogEvent?.Invoke(string.Format(source + ": " + message, args));
        }
    }
}
