﻿namespace DJH.SimConnectWrapper
{
    using Microsoft.FlightSimulator.SimConnect;

    internal enum DataStructure
    {
        FastUpdate,
        ManualUpdate
    }

    internal enum DataRequest
    {
        Slow,
        Manual
    };

    public interface ISimDataUpdate
    {
        /// <summary>
        /// Event fired on update
        /// </summary>
        event SimController_UpdateEvent OnUpdate;

        /// <summary>
        /// Register update events
        /// </summary>
        /// <param name="simconnect"></param>
        void RegisterUpdate(SimConnect simconnect);

        /// <summary>
        /// Emulate an update
        /// </summary>
        void Emulate(bool init);

        /// <summary>
        /// Update from sim data
        /// </summary>
        /// <param name="data">Sim Data</param>
        void Update(object[] data);
    }
}
