﻿namespace SimConnect
{
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
    using DJH.SimConnectWrapper;
    using Microsoft.FlightSimulator.SimConnect;
    using static SimConnectWrapper.SimEvents;

    /// <summary>
    /// Sim Connect Controller Wrapper
    /// 
    /// SimConnect docs are here:
    /// https://docs.microsoft.com/en-us/previous-versions/microsoft-esp/cc526980(v=msdn.10)
    /// </summary>
    public class SimConnectController : SimConnectControllerBase, ISimConnectController
    {
        /// <summary>
        /// SimConnect instance
        /// </summary>
        private SimConnect simconnect;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of application to register with FSX</param>
        public SimConnectController(string name) 
            : base(name)
        {
        }

        /// <summary>
        /// Get a value indicating whether SimConnect is connected to the simulation
        /// </summary>
        public override bool Connected
        {
            get
            {
                return simconnect != null;
            }
        }

        /// <summary>
        /// Handle window message
        /// </summary>
        /// <param name="m">message</param>
        /// <returns>true if handled</returns>
        public override bool HandleMessage(ref Message m)
        {
            try
            {
                if (m.Msg == SimDefs.WM_USER_SIMCONNECT)
                {
                    if (simconnect != null)
                    {
                        simconnect.ReceiveMessage();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                EventLog("HandleMessage", "Exception: {0}", ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Handle window message
        /// </summary>
        /// <param name="msg">message</param>
        /// <returns>true if handled</returns>
        public override bool HandleMessage(int msg)
        {
            try
            {
                if (msg == SimDefs.WM_USER_SIMCONNECT)
                {
                    if (simconnect != null)
                    {
                        simconnect.ReceiveMessage();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                EventLog("HandleMessage", "Exception: {0}", ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Connect to FSX
        /// </summary>
        /// <param name="hWnd">Window handle to receive events</param>
        public override void Connect(IntPtr hWnd)
        {
            try
            {
                if (simconnect == null)
                {
                    try
                    {
                        EventLog("Connect", "Connecting...");

                        // the constructor is similar to SimConnect_Open in the native API
                        simconnect = new SimConnect(appName, hWnd, SimDefs.WM_USER_SIMCONNECT, null, 0);

                        // listen to connect and quit msgs
                        simconnect.OnRecvOpen += new SimConnect.RecvOpenEventHandler(Simconnect_OnRecvOpen);
                        simconnect.OnRecvQuit += new SimConnect.RecvQuitEventHandler(Simconnect_OnRecvQuit);

                        // listen to events
                        simconnect.OnRecvEvent += new SimConnect.RecvEventEventHandler(Simconnect_OnRecvEvent);

                        // listen to exceptions
                        simconnect.OnRecvException += new SimConnect.RecvExceptionEventHandler(Simconnect_OnRecvException);

                        // catch a simobject data request
                        simconnect.OnRecvSimobjectDataBytype += new SimConnect.RecvSimobjectDataBytypeEventHandler(Simconnect_OnRecvSimobjectDataBytype);

                        // Send initial data request
                        InitDataRequest();
                    }
                    catch (COMException ex)
                    {
                        EventLog("Connect", "Unable to connect to FSX: {0}", ex.ErrorCode.ToString());
                    }
                }
                else
                {
                    EventLog("Connect", "Error - try again");
                    Close();
                }
            }
            catch (Exception ex)
            {
                EventLog("Connect", ex.Message);
            }
        }

        /// <summary>
        /// Temporary poller
        /// Will be replaced with internal thread at some point...
        /// </summary>
        public override void Poll()
        {
            try
            {
                simconnect.RequestDataOnSimObjectType(DataRequest.Slow, DataStructure.FastUpdate, 0, SIMCONNECT_SIMOBJECT_TYPE.USER);
            }
            catch (Exception ex)
            {
                EventLog("Poll", ex.Message);
                Close();
            }
        }

        /// <summary>
        /// Request manual update
        /// </summary>
        public override void ExecuteManualUpdate()
        {
            simconnect.RequestDataOnSimObjectType(DataRequest.Manual, DataStructure.ManualUpdate, 0, SIMCONNECT_SIMOBJECT_TYPE.USER);
        }

        /// <summary>
        /// Close connection to FSX
        /// </summary>
        public override void Close()
        {
            if (simconnect != null)
            {
                EventLog("Close", "Disconnecting...");

                // Dispose serves the same purpose as SimConnect_Close()
                simconnect.Dispose();
                simconnect = null;

                EventLog("Close", "Connection closed");
            }
            else
            {
                EventLog("Close", "Not connected...");
            }
        }

        /// <summary>
        /// Set up all the SimConnect related data definitions and event handlers 
        /// </summary>
        protected void InitDataRequest()
        {
            try
            {
                EventLog("InitDataRequest", "Initialising data request...");

                // subscribe to pitot heat switch toggle
                simconnect.MapClientEventToSimEvent(SimEvent.KEY_SIM_RESET, "SIM_RESET");
                simconnect.MapClientEventToSimEvent(SimEvent.EVENT_BRAKES, "BRAKES");
                simconnect.MapClientEventToSimEvent(SimEvent.EVENT_PAUSE, "PAUSE_TOGGLE");
                simconnect.MapClientEventToSimEvent(SimEvent.FLAPS_UP, "FLAPS_UP");
                simconnect.MapClientEventToSimEvent(SimEvent.FLAPS_DOWN, "FLAPS_DOWN");
                simconnect.MapClientEventToSimEvent(SimEvent.FLAPS_INC, "FLAPS_INCR");
                simconnect.MapClientEventToSimEvent(SimEvent.FLAPS_DEC, "FLAPS_DECR");

                simconnect.AddClientEventToNotificationGroup(NotificationGroup.GROUP0, SimEvent.KEY_SIM_RESET, false);
                simconnect.AddClientEventToNotificationGroup(NotificationGroup.GROUP0, SimEvent.EVENT_BRAKES, false);
                simconnect.AddClientEventToNotificationGroup(NotificationGroup.GROUP0, SimEvent.EVENT_PAUSE, false);
                simconnect.AddClientEventToNotificationGroup(NotificationGroup.GROUP0, SimEvent.FLAPS_UP, false);
                simconnect.AddClientEventToNotificationGroup(NotificationGroup.GROUP0, SimEvent.FLAPS_DOWN, false);
                simconnect.AddClientEventToNotificationGroup(NotificationGroup.GROUP0, SimEvent.FLAPS_INC, false);
                simconnect.AddClientEventToNotificationGroup(NotificationGroup.GROUP0, SimEvent.FLAPS_DEC, false);

                // set the group priority
                simconnect.SetNotificationGroupPriority(NotificationGroup.GROUP0, SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST);

                // Register update handlers
                SlowUpdate.RegisterUpdate(simconnect);
                ManualUpdate.RegisterUpdate(simconnect);
            }
            catch (COMException ex)
            {
                EventLog("InitDataRequest", ex.Message);
            }
        }

        /// <summary>
        /// SimConnect Exception handler
        /// </summary>
        /// <param name="sender">Message Sender</param>
        /// <param name="data">Exception data</param>
        private void Simconnect_OnRecvException(SimConnect sender, SIMCONNECT_RECV_EXCEPTION data)
        {
            EventLog("RECV_EXCEPTION", ((SimDefs.SIMCONNECT_EXCEPTION)data.dwException).ToString());
        }

        /// <summary>
        /// SimConnect Event handler
        /// </summary>
        /// <param name="sender">Message Sender</param>
        /// <param name="data">Event data</param>
        private void Simconnect_OnRecvEvent(SimConnect sender, SIMCONNECT_RECV_EVENT recEvent)
        {
            EventLog("RECV_EVENT", ((SimEvent)recEvent.uEventID).ToString());
        }

        /// <summary>
        /// SimConnect Quit handler
        /// </summary>
        /// <param name="sender">Message Sender</param>
        /// <param name="data">Quit data</param>
        private void Simconnect_OnRecvQuit(SimConnect sender, SIMCONNECT_RECV data)
        {
            EventLog("RECV_QUIT", "FSX Disconnected");
        }

        /// <summary>
        /// SimConnect Open handler
        /// </summary>
        /// <param name="sender">Message Sender</param>
        /// <param name="data">Open data</param>
        private void Simconnect_OnRecvOpen(SimConnect sender, SIMCONNECT_RECV_OPEN data)
        {
            EventLog("RECV_OPEN", "Connected to FSX");
        }

        /// <summary>
        /// SimConnect SimObject data handler
        /// </summary>
        /// <param name="sender">Message Sender</param>
        /// <param name="data">SimObject data</param>
        private void Simconnect_OnRecvSimobjectDataBytype(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA_BYTYPE data)
        {
            switch ((DataRequest)data.dwRequestID)
            {
                case DataRequest.Slow:
                    SlowUpdate.Update(data.dwData);
                    break;
                case DataRequest.Manual:
                    ManualUpdate.Update(data.dwData);
                    break;
                default:
                    break;
            }
        }
    }
}
